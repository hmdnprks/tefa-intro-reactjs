import logo from './logo.svg';
import './App.css';
import React, { useState } from 'react';
import ButtonComponent from './components/Button/Button';
import Header from './components/HeaderFunctional/Header';

function App() {
  const [number, setNumber] = useState(0);
  const [header, setHeader] = useState(true);


  return (
    <div className="App">
      <header className="App-header">
        <p>Contoh nomor: {number}</p>
        {header && (<Header count={10} number={50} changeNumber={() => setNumber(100)}/>)}
        <ButtonComponent />
        <button onClick={() => setHeader(!header)}>Toggle header</button>
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
