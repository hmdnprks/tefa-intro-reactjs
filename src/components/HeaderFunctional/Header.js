import React, { useEffect, useState } from 'react';

const Header = (props) => {
  console.log('render');
  const { count: counter, changeNumber } = props;
  const [count, setCount] = useState(counter);
  const [countPlus, setCountPlus] = useState(counter);

  useEffect(() => {
    return () => {
      console.log('goodbye')
    }
  }, [])

  return (
    <>
      <p>Count plus : {countPlus}</p>
      <button onClick={() => setCount(count + 1)} >Increment Button</button>
      <p>Counter : {count}</p>
      <button onClick={changeNumber}>Ganti nomor</button>
    </>
  )
}

export default Header;