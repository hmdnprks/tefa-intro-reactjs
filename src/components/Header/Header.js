import React from 'react';

class Header extends React.Component {
  constructor(props){
    console.log('constructor');
    super(props);
    console.log(this.props);
    this.state = {
      count: 0
    }
  }

  componentWillMount(){
    console.log('componentwillmount');
  }

  componentDidMount() {
    console.log('componentDidMount');
  }

  componentWillUnmount() {
    console.log('goofbye');
  }

  render() {
    console.log('render');
    const { count } = this.state;
    const { count: counter } = this.props;
    return (
      <>
        <p>Nilai props adalah {counter}</p>
        <button onClick={() => this.setState({ count: count + 1 })} >Increment</button>
        <p>Counter : {count}</p>
      </>
    )
  }
}

export default Header;